const User = require("./../models/User");

/* module.exports.createUser = function (reqBody) {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    userName: reqBody.userName,
    password: reqBody.password,
  });

  return newUser.save().then((savedUser, error) => {
    if (error) {
      return error;
    } else {
      return savedUser;
    }
  });
};

module.exports.allUsers = function () {
  return User.find({}).then((savedUser, error) => {
    if (error) {
      return error;
    } else {
      return savedUser;
    }
  });
}; */

module.exports = {
  createUser: (reqBody) => {
    let newUser = new User({
      firstName: reqBody.firstName,
      lastName: reqBody.lastName,
      userName: reqBody.userName,
      password: reqBody.password,
    });

    return newUser.save().then((savedUser, error) => {
      if (error) {
        return error;
      } else {
        return savedUser;
      }
    });
  },

  allUsers: () => {
    return User.find({}).then((savedUser, error) => {
      if (error) {
        return error;
      } else {
        return savedUser;
      }
    });
  },

  oneUser: (id) => {
    return User.findById(id).then((user) => user);
  },

  updateOneUser: (id, update) => {
    return User.findByIdAndUpdate(id, update, { new: true }).then(
      (user, error) => {
        if (error) {
          return error;
        } else {
          return user;
        }
      }
    );
  },

  deleteOne: (id) => {
    return User.findByIdAndDelete(id).then((user) => user);
  },
};
