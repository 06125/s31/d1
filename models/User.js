const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  userName: String,
  password: String,
  timeStamp: { type: String, default: new Date() },
});

module.exports = mongoose.model("User", userSchema);
