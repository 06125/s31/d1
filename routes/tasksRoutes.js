const express = require("express");
const User = require("./../models/User");
const userController = require("./../controllers/userController");
const router = express.Router();

// Insert a new task
router.post("/users/add", (req, res) => {
  /* let newUser = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    userName: req.body.userName,
    password: req.body.password,
  });

  newUser.save((err, savedUser) => {
    if (err) {
      res.send(err);
    } else {
      res.send(`New user saved: ${savedUser}`);
    }
  }); */

  userController.createUser(req.body).then((result) => res.send(result));
});

router.get("/users", (req, res) => {
  /*  User.find({}, (error, users) => {
    if (error) {
      console.log(error);
    } else {
      res.send(users);
    }
  }); */

  userController.allUsers().then((result) => res.send(result));
});

router.get("/users/:id", (req, res) => {
  /* User.findById(req.params.id, (error, user) => {
    if (error) {
      console.log(error);
    } else {
      res.send(user);
    }
  }); */

  userController.oneUser(req.params.id).then((result) => res.send(result));
});

router.put("/users/:id", (req, res) => {
  /* User.findByIdAndUpdate(
    req.params.id,
    req.body,
    { new: true },
    (error, user) => {
      if (error) {
        console.log(error);
      } else {
        res.send(user);
      }
    }
  ); */

  userController
    .updateOneUser(req.params.id, req.body)
    .then((result) => res.send(result));
});

router.delete("/users/:id", (req, res) => {
  /* User.findByIdAndDelete(req.params.id, (error, user) => {
    if (error) {
      console.log(error);
    } else {
      res.send(user);
    }
  }); */

  userController.deleteOne(req.params.id).then((result) => res.send(result));
});

/* router.delete("/users/many", (req, res) {
    User.deleteMany()
}) */

module.exports = router;
