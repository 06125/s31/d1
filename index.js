const express = require("express");
const mongoose = require("mongoose");
const app = express();
const PORT = 3000;

// from router
const tasksRoutes = require("./routes/tasksRoutes");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose
  .connect(
    "mongodb+srv://eclespinosa:emilmongo@cluster0.vtyel.mongodb.net/s31?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => {
    console.log("Connected to Database");
  })
  .catch((error) => {
    console.log(error);
  });

app.use("/", tasksRoutes);

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
